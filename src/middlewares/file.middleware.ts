import multer = require("multer");
const upload = multer({ storage: multer.memoryStorage() });
export const fileMiddleware = upload.single("file");
