import { HttpError } from "../utils/error";
import { ErrorRequestHandler } from "express";

export const errorMiddleware: ErrorRequestHandler = (err, req, res, next) => {
  if (err instanceof HttpError) {
    const statusCode = err.detail.httpCode || 500;
    const message = err.message || "Internal server error";
    const code = err.detail.code || "Internal server error";
    return res.status(statusCode).json({ message, code });
  }
  console.error(err);
  return res.status(500).json({ message: err.message, ...err });
};
