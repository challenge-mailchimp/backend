import dotenv = require("dotenv");
dotenv.config();
import express = require("express");
import { connectDatabase } from "./utils/database.connection";
import { IRoute } from "./interfaces/user.interface";
import { errorMiddleware } from "./middlewares/error.middleware";

export class App {
  private app = null;
  static server = null;
  static instance = null;

  constructor(routes: IRoute[]) {
    this.app = express();
    this.startApp(routes);
  }
  private async startApp(routes: IRoute[]) {
    await this.initDatabase();
    await this.registerRouters(routes);
    await this.startServer();
  }
  static getInstance(routes: IRoute[]) {
    if (!this.instance) {
      return (this.instance = new App(routes));
    }
    this.instance;
  }
  registerRouters(routes: IRoute[]) {
    this.app.use(express.urlencoded());
    routes.forEach((route) => this.app.use("/api/v1", route.route));
    this.app.use(errorMiddleware)
    console.log("===== REGISTER ROUTES OK");
  }
  async initDatabase() {
    await connectDatabase();
    console.log("===== DATABASE OK");
  }
  async startServer() {
    return new Promise((resolve, reject) => {
      App.server = this.app.listen(process.env.HTTP_PORT ?? 3001, () => {
        console.log(
          "===== SERVER OK AT http://localhost:" + process.env.HTTP_PORT ??
            3001
        );
        resolve(true);
      });
    });
  }
  static getServer() {
    return this.server;
  }
}
