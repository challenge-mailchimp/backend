import { Router } from "express";
import { IRoute } from "@app/interfaces/user.interface";
import { UserController } from "@app/controllers/user.controller";
import { fileMiddleware } from "../middlewares/file.middleware";
export class UserRoute implements IRoute {
  public route = Router();
  private path = "/users";
  constructor(private readonly userController: UserController) {
    this.initRoutes();
  }

  initRoutes() {
    this.route.post(
      this.path + "/import",
      fileMiddleware,
      this.userController.importCsv
    );

    this.route.get(this.path + "/hook", this.userController.hookMailchimp);
    this.route.get(this.path , this.userController.get);
  }
}
