import { IPayloadCreateContact } from "@app/interfaces/user.interface";
import mailchimp from "@mailchimp/mailchimp_marketing";
export interface IMailChimp {
  getConnection(): void;
  addEmai(data: any): void;
}
export default class MailChimpService implements IMailChimp {
  getConnection() {
    mailchimp.setConfig({
      apiKey: process.env.MAILCHIMP_TOKEN,
      server: process.env.MAILCHIMP_SERVER,
    });
  }
  async addEmai(data: IPayloadCreateContact) {
    try {
      const response = await mailchimp.lists.addListMember(
        process.env.MAILCHIMP_LIST_ID,
        {
          email_address: data.email_address,
          status: data.status,
          merge_fields: data.merge_fields,
        }
      );
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  }
}
