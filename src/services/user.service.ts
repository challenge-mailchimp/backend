import { isUUID } from "../utils/isUUID";
import Address from "../models/address.model";
import Email from "../models/email.model";
import Phone from "../models/phone.model";
import User from "../models/user.model";
import csv = require("csvtojson");
export default class UserService {
  private records = [];
  constructor(private readonly buffer: ArrayBufferLike) {}
  async parseToJson() {
    this.records = await csv({
      noheader: true,
      output: "csv",
    }).fromString(this.buffer.toString());
  }
  async bulkCreate() {
    for (let record of this.records) {
      let [
        firstName,
        lastName,
        id,
        updatedAt,
        email,
        emailUpdatedAt,
        visitedToday,
        visitedTodayDate,
        first_line,
        second_line,
        city,
        zip,
        state,
        country,
        phone,
        phoneUpdatedAt,
      ] = record;
      if (!isUUID(id)) {
        console.log("RECORD IS NOT UUID", { record });
        continue;
      }
      try {
        let address = await Address.create({
          first_line,
          second_line,
          city,
          zip,
          state,
          country,
        });
        if (phone) {
          phone = await Phone.create({
            phone,
            updatedAt: this.getValidDateOrNull(phoneUpdatedAt),
          });
        }
        if (email) {
          email = await Email.create({
            email,
            updatedAt: this.getValidDateOrNull(emailUpdatedAt),
          });
        }
        await User.create({
          id,
          firstName,
          lastName,
          visitedToday: visitedToday == "Yes",
          visitedTodayDate: this.getValidDateOrNull(visitedTodayDate),
          updatedAt: this.getValidDateOrNull(updatedAt),
          address: this.getValidOrNull(address),
          phone: this.getValidOrNull(phone),
          email: this.getValidOrNull(email),
        });
        console.log("Record created", {id})
      } catch (error) {
        console.error({ record, error });
        return;
      }
    }
  }
  private getValidDateOrNull(value: any) {
    return !!value
      ? (new Date(value) as any) == "Invalid Date"
        ? null
        : new Date(value)
      : null;
  }
  private getValidOrNull(value: any) {
    return !!value ? value : null;
  }
}
