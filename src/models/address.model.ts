import mongoose, { Document, Schema } from "mongoose";
import { IUser } from "./user.model";

export interface IAddress extends Document {
  first_line: string;
  second_line: string;
  city: string;
  zip: string;
  state: string;
  country: string;
}

const schema = new Schema<IAddress>({
  first_line: { type: String, required: false },
  second_line: { type: String, required: false },
  city: { type: String, required: false },
  zip: { type: String, required: false },
  state: { type: String, required: false },
  country: { type: String, required: false },
});

const Address = mongoose.model<IAddress>("Address", schema);
export default Address;
