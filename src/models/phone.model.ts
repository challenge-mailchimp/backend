import mongoose, { Document, Schema } from "mongoose";
import { IUser } from "./user.model";

interface IPhone extends Document {
  phone: string;
  updatedAt: Date;
}

const schema = new Schema<IPhone>({
  phone: { type: String, required: true },
  updatedAt: { type: Date, required: false },
});

export default mongoose.model<IPhone>("Phone", schema);
