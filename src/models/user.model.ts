import mongoose, { Document, Schema } from "mongoose";

export interface IUser extends Document {
  firstName: string;
  lastName: string;
  visitedToday: boolean;
  visitedTodayDate: Date;
  updatedAt: Date;
  createdAt: Date;
  email: any;
  phone: any;
  address: any;
}

const visitorSchema = new Schema<IUser>({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  visitedToday: { type: Boolean, required: false },
  visitedTodayDate: { type: Date, required: false },
  updatedAt: { type: Date },
  email: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: "Email",
  },
  phone: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: "Phone",
  },
  address: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: "Address",
  },
});

const User = mongoose.model<IUser>("User", visitorSchema);

export default User;
