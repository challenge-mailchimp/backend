import mongoose, { Document, Schema } from "mongoose";
import { IUser } from "./user.model";

interface IEmail extends Document {
  email: string;
  updatedAt: Date;
}

const schema = new Schema<IEmail>({
  email: { type: String, required: true },
  updatedAt: { type: Date, required: false },
});

export default mongoose.model<IEmail>("Email", schema);
