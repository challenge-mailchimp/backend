import { App } from "./app";
import { UserController } from "./controllers/user.controller";
import { UserRoute } from "./routes/user.route";

App.getInstance([new UserRoute(new UserController())]);
