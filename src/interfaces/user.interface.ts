import { Request, Router } from "express";

export interface IEmailModel {}

export interface IUserRepository {
  findAll(): IEmailModel[];
}

export interface IRoute {
  route: Router;
}
export interface RequestWithFile extends Request {
  file: Buffer;
}

export interface IPayloadCreateContact {
  email_address: string;
  status:        string;
  merge_fields:  MergeFields;
}

export interface MergeFields {
  FNAME: string;
  LNAME: string;
}