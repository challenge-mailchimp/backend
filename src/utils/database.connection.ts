import mongoose from "mongoose";
export async function connectDatabase() {
  try {
    await mongoose.connect(process.env.MONGODB_URI);
  } catch (error) {
    console.log("Error al conectar con la base de datos", error);
  }
}
