interface IObserver {
  update(): void;
}

interface ISubject {
  subscribe(observer: IObserver): void;
  notify(): void;
}

export class Subject implements ISubject {
  private observers: IObserver[] = [];

  public subscribe(observer: IObserver): void {
    this.observers.push(observer);
  }

  public notify(): void {
    for (const observer of this.observers) {
      observer.update();
    }
  }
}

export const UserSubject = new Subject()
