interface PayloadResponse {
  httpCode: number;
  code: string;
  message: string;
}
interface ResponseCode {
  [key: string]: PayloadResponse;
}
export const ErrorCodes: ResponseCode = {
  FILE_NOT_FOUND: {
    httpCode: 400,
    message: "File should not be empty",
    code: "F001",
  },
};
export class HttpError extends Error {
  constructor(readonly detail: PayloadResponse) {
    super(detail.message);
  }
}
