import { RequestWithFile } from "@app/interfaces/user.interface";
import { ErrorCodes, HttpError } from "../utils/error";
import { NextFunction, Request, Response } from "express";
import UserService from "../services/user.service";
import User from "../models/user.model";

export class UserController {
  constructor() {}
  public importCsv = async (
    req: RequestWithFile,
    res: Response,
    next: NextFunction
  ) => {
    try {
      if (!req.file) {
        throw new HttpError(ErrorCodes.FILE_NOT_FOUND);
      }
      const userService = new UserService(req.file.buffer);
      await userService.parseToJson();
      await userService.bulkCreate();
      res.sendStatus(204);
    } catch (error) {
      next(error);
    }
  };

  public hookMailchimp = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      console.log({ body: req.body });
    } catch (error) {
      next(error);
    }
  };

  public get = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const users = await User.find().populate("email phone address");
      res.json({ data: users, count: users.length });
    } catch (error) {
      next(error);
    }
  };
}
